
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Cedric Nugteren <cedric.nugteren@surfsara.nl>
#
# ==================================================================================================

# CMake project
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)
project("3_spectral" CXX)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/common.cmake)

# ==================================================================================================

# Dwarf 3: Spectral methods 
message("--------------------")
message("Dwarf 3: Spectral methods:")
message("--------------------")
set(DWARF_PREFIX 3_spectral) # The prefix of the name of the binaries produced

# Packages are optional: if they are not present, certain code samples are not compiled
find_package(OpenMP)   # Built-in in CMake
find_package(MPI)      # Built-in in CMake
find_package(CUDA)     # Built-in in CMake
find_package(OpenCL)   # Included as ${CMAKE_MODULE_PATH}/FindOpenCL.cmake
find_package(FFTW)     # Included as ${CMAKE_MODULE_PATH}/FindFFTW.cmake

# ==================================================================================================

# C++ compiler settings
find_package(Common)
select_compiler_flags(cxx_flags
  GNU "-O3 -march=native"   # I suggest remove "-O3" as this is controlled by the CMAKE_BUILD_TYPE
  CLANG "-O3 -march=native" # same here
  Intel "-axavx2,avx")
set(CXX_FLAGS ${cxx_flags})
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set(CXX_FLAGS "${CXX_FLAGS} -Wall -Wno-comment")
  if(APPLE)
    set(CXX_FLAGS "${CXX_FLAGS} -Wa,-q")
  endif()
endif()
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS}")

# NVCC compiler settings
if (CUDA_FOUND)
  set(CUDA_PROPAGATE_HOST_FLAGS OFF)
  set(CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} -O3")
  set(CUDA_HOST_COMPILER "g++")
endif()

# ==================================================================================================

# Add the examples
include(fftw.cmake)
include(cufft.cmake)

# ==================================================================================================
